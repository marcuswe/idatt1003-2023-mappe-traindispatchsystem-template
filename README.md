# Portfolio project IDATA1003 - 2023
This file uses Mark Down syntax. For more information see [here](https://www.markdownguide.org/basic-syntax/).

STUDENT NAME = "Marcus Westum"  
STUDENT ID = "566244"

## Project description

[//]: # (TODO: Write a short description of your project/product here.)
This project is part of a final assessment in IDATT1003 Programmering 1. The project
is about creating a simplified system handling train departures. Some of the tasks 
the application is supposed to handle includes; showing registered departures, register
a new departure, finding a departure by a given train number, and more. Some of the core
principles followed includes 'cohesion' and 'coupling', as well as creating tests in order 
to assure robust classes.

## Project structure

[//]: # (TODO: Describe the structure of your project here. How have you used packages in your structure. Where are all sourcefiles stored. Where are all JUnit-test classes stored. etc.)
The project consists of two packages created as a Maven project. The two packages consists 
of a couple of classes. The first package consists of the program itself, while the other 
includes two testclasses used to make more robust classes in the main folder. The project itself 
has been stored via commits and pushes to GitLab, as well as locally on my computer.

## Link to repository

[//]: # (TODO: Include a link to your repository here.)
https://gitlab.stud.idi.ntnu.no/marcuswe/idatt1003-2023-mappe-traindispatchsystem-template

## How to run the project

[//]: # (TODO: Describe how to run your project here. What is the main class? What is the main method?
What is the input and output of the program? What is the expected behaviour of the program?)
The project is run via the TrainDispatchApp class where the main method is located. 
The main method is the entry point of the application, and starts by creating a new instance of 
the UserInterface class and initializes it by passing a new instance og the TrainDepartureRegister class.
The application interacts with a user, and handles input through different methods that has 
been created. The output is printed out to the console, and mainly consists of information 
about registered train departures, but also error messages giving the user information on how to 
correctly give valid input.

## How to run the tests

[//]: # (TODO: Describe how to run the tests here.)
The tests can either be run separately by clicking a green triangle to the left of each test,
and choosing "Run 'test...'", or it can all be run simultaneously by choosing "current file" and 
"Run 'TrainDepartureTest' or 'TrainDepartureRegisterTest'".

## References

[//]: # (TODO: Include references here, if any. For example, if you have used code from the course book, include a reference to the chapter.
Or if you have used code from a website or other source, include a link to the source.)
