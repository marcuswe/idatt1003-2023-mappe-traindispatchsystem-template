package edu.ntnu.stud;

import java.time.Duration;
import java.time.LocalTime;

/**
 * This class represents a train departure.
 * It provides information about the departure time, line, train number,
 * destination, track and delay.
 */
public class TrainDeparture implements Comparable<TrainDeparture> {
  private final LocalTime departureTime;
  private final String line;
  private final int trainNumber;
  private final String destination;
  private int track;
  private Duration delay;

  /**
   * Constructs a TrainDeparture.
   *
   * @param departureTime The time of departure.
   * @param line The line the train is on.
   * @param trainNumber The train number.
   * @param destination The destination of the train.
   * @param delay The delay of the train.
   * @throws IllegalArgumentException If delay is negative.
   */
  public TrainDeparture(LocalTime departureTime, String line, int trainNumber,
                        String destination, Duration delay) {
    this.departureTime = departureTime;
    this.line = line;
    this.trainNumber = trainNumber;
    this.destination = destination;
    if (delay.isNegative()) {
      throw new IllegalArgumentException("Delay cannot be negative.");
    }
    this.delay = delay;
    this.track = -1;
  }

  /**
   * Returns the departure time of the train.
   *
   * @return The departure time of the train.
   */
  public LocalTime getDepartureTime() {
    return departureTime.plus(delay);
  }

  /**
   * Returns the original departure time of the train.
   *
   * @return The original departure time of the train.
   */
  public LocalTime getOriginalDepartureTime() {
    return departureTime;
  }

  /**
   * Returns the line the train is on.
   *
   * @return The line the train is on.
   */
  public String getLine() {
    return line;
  }

  /**
   * Returns the train number.
   *
   * @return The train number.
   */
  public int getTrainNumber() {
    return trainNumber;
  }

  /**
   * Returns the destination of the train.
   *
   * @return The destination of the train.
   */
  public String getDestination() {
    return destination;
  }

  /**
   * Returns the track the train is on.
   *
   * @return The track the train is on.
   */
  public int getTrack() {
    return track;
  }

  /**
   * Sets the track of the train.
   *
   * @param track The track the train is on.
   * @throws IllegalArgumentException If track is negative.
   */
  public void setTrack(int track) {
    if (track < 0) {
      throw new IllegalArgumentException("Track cannot be negative.");
    }
    this.track = track;
  }

  /**
   * Returns the delay of the train.
   *
   * @return The delay of the train.
   */
  public Duration getDelay() {
    return delay;
  }

  /**
   * Sets the departure time of the train.
   *
   * @param delay The delay of the train.
   */
  public void setDelay(Duration delay) {
    this.delay = delay;
  }

  /**
   * Returns a string representation of the train departure.
   *
   * @return A string representation of the train departure.
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    if (delay.isZero()) {
      sb.append(departureTime).append(" | ").append(line).append(" | ")
            .append(trainNumber).append(" | ").append(destination);
    } else {
      sb.append(departureTime).append(" | ").append(line).append(" | ")
            .append(trainNumber).append(" | ").append(destination).append(" | ")
            .append(delay.toMinutes()).append(" min delayed");
    }
    if (track != -1) {
      sb.append(" | Track: ").append(track);
    }
    return sb.toString();
  }

  /**
   * Compares this TrainDeparture with the specified TrainDeparture for order.
   * Help from ChatGPT
   *
   * @param other the object to be compared.
   * @return a negative integer, zero, or a positive integer as this object is less than,
   *         equal to, or greater than the specified object.
   */
  @Override
  public int compareTo(TrainDeparture other) {
    return this.getDepartureTime().compareTo(other.getDepartureTime());
  }
}
