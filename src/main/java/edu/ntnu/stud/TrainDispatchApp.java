package edu.ntnu.stud;

/**
 * This is the main class for the train dispatch application.
 */
public class TrainDispatchApp {

  /**
   * The main method of the application.
   * Creates a new UserInterface and starts the application.
   *
   * @param args The command line arguments.
   */
  public static void main(String[] args) {
    UserInterface ui = new UserInterface();
    ui.init(new TrainDepartureRegister());
    ui.start();
  }
}
