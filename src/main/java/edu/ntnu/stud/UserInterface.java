package edu.ntnu.stud;

import java.time.DateTimeException;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * This class represents the user interface for the train departure register.
 * It provides methods for starting the application and handling user input.
 * It also provides methods for showing the menu and handling the user's choice.
 */
public class UserInterface {

  private TrainDepartureRegister trainDepartureRegister;

  private Scanner sc;

  /**
   * Initializes the user interface.
   * Sets the train departure register and creates a new scanner.
   *
   * @param trainDepartureRegister The train departure register to use.
   */
  public void init(TrainDepartureRegister trainDepartureRegister) {
    this.trainDepartureRegister = trainDepartureRegister;
    this.sc = new Scanner(System.in);
  }

  /**
   * Starts the train departure application, displaying the menu and handling user input.
   * The method also registers three train departures for testing purposes.
   */
  public void start() {
    TrainDeparture trainDeparture1 = new TrainDeparture(LocalTime.of(12, 0), "L1", 231,
            "Oslo", Duration.ZERO);
    TrainDeparture trainDeparture2 = new TrainDeparture(LocalTime.of(13, 15), "A2", 142,
            "Trondheim", Duration.ZERO);
    TrainDeparture trainDeparture3 = new TrainDeparture(LocalTime.of(14, 30), "L23", 153,
            "Bergen", Duration.ZERO);
    trainDepartureRegister.registerTrainDeparture(trainDeparture1);
    trainDepartureRegister.registerTrainDeparture(trainDeparture2);
    trainDepartureRegister.registerTrainDeparture(trainDeparture3);
    boolean finished = false;
    while (!finished) {
      try {
        showMenu();
        choice(sc.nextInt());
      } catch (InputMismatchException e) {
        System.out.println("Invalid input. Please enter a number.");
        sc.nextLine();
      }
    }
  }

  /**
   * Displays the menu.
   */
  public void showMenu() {

    System.out.println("\n1. Show all departures.");
    System.out.println("2. Update the current time.");
    System.out.println("3. Register new departure.");
    System.out.println("4. Assign track to departure.");
    System.out.println("5. Assign delay to departure.");
    System.out.println("6. Find departure by train number.");
    System.out.println("7. Find departure by destination.");
    System.out.println("8. Exit program.");
    System.out.println("Enter choice: ");
  }

  /**
   * Handles the user's choice.
   * Used GitHub Copilot to generate this method as it saves time.
   *
   * @param choice The user's choice.
   */
  public void choice(int choice) {
    switch (choice) {
      case 1:
        showAllDepartures();
        break;
      case 2:
        updateCurrentTime();
        break;
      case 3:
        registerNewDeparture();
        break;
      case 4:
        assignTrackToDeparture();
        break;
      case 5:
        assignDelayToDeparture();
        break;
      case 6:
        findDepartureByTrainNumber();
        break;
      case 7:
        findDepartureByDestination();
        break;
      case 8:
        System.exit(0);
        break;
      default:
        System.out.println("Invalid choice. Please choose a number between 1 and 8.");
        break;
    }
  }

  /**
   * Displays information about all departures.
   */
  private void showAllDepartures() {
    if (trainDepartureRegister.getManualTime() == null) {
      System.out.println("Manual time not set."
              + " Please set a time using the 'Update the current time' command.");
      return;
    }
    trainDepartureRegister.removeExpiredDepartures();
    Collections.sort(trainDepartureRegister.getTrainDepartures());
    System.out.println("\nAll departures:");
    for (TrainDeparture trainDeparture : trainDepartureRegister.getTrainDepartures()) {
      System.out.println(trainDeparture.toString());
    }
  }

  /**
   * Updates the current time based on user input.
   */
  private void updateCurrentTime() {
    boolean input = false;
    while (!input) {
      sc.nextLine();
      System.out.println("\nEnter new time (hh:mm): ");
      String time = sc.nextLine();
      try {
        LocalTime newTime = LocalTime.parse(time);
        if (!trainDepartureRegister.firstManualTimeSet()
                || newTime.isAfter(trainDepartureRegister.getManualTime())) {
          trainDepartureRegister.setManualTime(newTime);
          System.out.println("Current time updated.");
          input = true;
        } else {
          System.out.println("New time is before current time."
                  + " New time must be after the previously set time. Press enter to try again.");
        }
      } catch (Exception e) {
        System.out.println("Invalid time format, must be hh:mm. Press enter to try again.");
      }
    }
  }

  /**
   * Registers a new departure based on user input.
   * Help from ChatGPT.
   */
  private void registerNewDeparture() {
    boolean input = false;
    while (!input) {
      try {
        sc.nextLine();
        System.out.println("\nEnter departure time (hh:mm): ");
        String time = sc.nextLine();
        LocalTime newTime = LocalTime.parse(time);
        System.out.println("Enter line: ");
        String line = sc.nextLine();
        if (trainDepartureRegister.hasDuplicateDepartureTimeAndLine(newTime, line)) {
          System.out.println("There already exists another train with "
                  + "the same departure time and line. Press enter to try again.");
          continue;
        }
        System.out.println("Enter train number: ");
        int trainNumber = sc.nextInt();
        sc.nextLine();
        System.out.println("Enter destination: ");
        String destination = sc.nextLine();
        System.out.println("Enter delay (hh:mm): ");
        String delay = sc.nextLine();
        Duration newDelay = Duration.parse("PT" + delay.replace(":", "H") + "M");
        TrainDeparture newTrainDeparture = new TrainDeparture(newTime, line, trainNumber,
                destination, newDelay);
        boolean success = trainDepartureRegister.registerTrainDeparture(newTrainDeparture);
        if (success) {
          System.out.println("New departure registered.");
          System.out.println("Size of trainDepartures: " + trainDepartureRegister
                  .getTrainDepartures().size());
          input = true;
        } else {
          System.out.println("There already exists another train with "
                  + "the same train number. Press enter to try again.");
        }
      } catch (Exception e) {
        System.out.println("Invalid time format, must be hh:mm. Press enter to try again.");
      }
    }
  }

  /**
   * Assigns a track to a departure based on user input.
   */
  private void assignTrackToDeparture() {
    boolean input = false;
    while (!input) {
      try {
        System.out.println("\nEnter train number: ");
        int trainNumber = sc.nextInt();
        sc.nextLine();
        System.out.println("Enter track: ");
        int track = sc.nextInt();
        sc.nextLine();
        trainDepartureRegister.assignTrack(trainNumber, track);
        input = true;
      } catch (InputMismatchException e) {
        System.out.println("Invalid input, must be a valid integer/whole number."
                + " Press enter to try again.");
        sc.nextLine();
      }
    }
  }

  /**
   * Assigns a delay to a departure based on user input.
   */
  private void assignDelayToDeparture() {
    System.out.println("\nEnter train number: ");
    int trainNumber;
    try {
      trainNumber = sc.nextInt();
      sc.nextLine();
    } catch (InputMismatchException e) {
      System.out.println("Invalid input. Please enter a valid train number.");
      sc.nextLine();
      return;
    }
    System.out.println("Enter delay (minutes): ");
    int delayInMinutes;
    try {
      delayInMinutes = sc.nextInt();
      sc.nextLine();
      try {
        Duration delay = Duration.ofMinutes(delayInMinutes);
        trainDepartureRegister.assignDelay(trainNumber, delay);
      } catch (IllegalArgumentException e) {
        System.out.println("Error: " + e.getMessage());
      }
    } catch (InputMismatchException e) {
      System.out.println("Invalid input. Please enter a valid integer/whole number.");
      sc.nextLine();
    } catch (DateTimeException e) {
      System.out.println("Error" + e.getMessage());
    }
  }


  /**
   * Finds a departure by its train number based on user input.
   */
  private void findDepartureByTrainNumber() {
    int trainNumber;
    try {
      System.out.println("\nEnter train number: ");
      trainNumber = sc.nextInt();
      sc.nextLine();
    } catch (InputMismatchException e) {
      System.out.println("Invalid input. Please enter a valid integer/whole number.");
      sc.nextLine();
      return;
    }
    TrainDeparture trainDeparture = trainDepartureRegister.findDepartureByTrainNumber(trainNumber);
    if (trainDeparture != null) {
      System.out.println(trainDeparture);
    } else {
      System.out.println("No departure found.");
    }
  }

  /**
   * Finds all departures with a given destination based on user input.
   */
  private void findDepartureByDestination() {
    sc.nextLine();
    System.out.println("\nEnter destination: ");
    String destination = sc.nextLine();
    ArrayList<TrainDeparture> departures = trainDepartureRegister
            .findDeparturesByDestination(destination);
    if (departures.isEmpty()) {
      System.out.println("No departures found.");
    } else {
      for (TrainDeparture trainDeparture : departures) {
        System.out.println(trainDeparture.toString());
      }
    }
  }
}
