package edu.ntnu.stud;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * This class represents a register of train departures.
 * The class provides methods for registering, assigning tracks and delays.
 * It also manages the schedule, tracks, delays, and manual time for registered train departures.
 */
public class TrainDepartureRegister {

  /**
   *List containing registered train departures.
   */
  private ArrayList<TrainDeparture> trainDepartures = new ArrayList<>();

  /**
   * Map containing the schedule for registered train numbers.
   */
  private Map<Integer, Integer> schedule;

  /**
   * Map containing the delays for registered train numbers.
   */
  private Map<Integer, Duration> delays;

  /**
   * The manually set time used for tracking train departures.
   * Help from CHatGPT.
   */
  private LocalTime manualTime;

  /**
   * Boolean indicating whether the manual time has been set or not.
   * Help from ChatGPT.
   */
  private boolean firstManualTimeSet = false;

  /**
   * Default constructor for the TrainDepartureRegister class.
   * Initializes the schedule and delays maps.
   */
  public TrainDepartureRegister() {
    this.schedule = new HashMap<>();
    this.delays = new HashMap<>();
  }

  /**
   * Method for registering a new train departure.
   * Checks for duplicate train numbers before registering a new departure.
   *
   * @param newTrainDeparture The train departure to register.
   * @return True if the train departure was registered successfully, false otherwise.
   */
  public boolean registerTrainDeparture(TrainDeparture newTrainDeparture) {
    for (TrainDeparture trainDeparture : trainDepartures) {
      if (trainDeparture.getTrainNumber() == newTrainDeparture.getTrainNumber()) {
        return false;
      }
    }
    trainDepartures.add(newTrainDeparture);
    schedule.put(newTrainDeparture.getTrainNumber(), -1);
    return true;
  }

  /**
   * Method for checking if there exists a train departure with the same departure time and line.
   *
   * @param time The time to check for duplicates.
   * @param line The line to check for duplicates.
   * @return True if there exists a train departure with the same departure time and line,
   *        false otherwise.
   */
  public boolean hasDuplicateDepartureTimeAndLine(LocalTime time, String line) {
    for (TrainDeparture trainDeparture : trainDepartures) {
      if (trainDeparture.getDepartureTime().equals(time) && trainDeparture.getLine().equals(line)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Method for assigning a track to a train departure.
   *
   * @param trainNumber The train number to check for.
   * @param track The track to check for.
   */
  public void assignTrack(int trainNumber, int track) {
    boolean found = false;
    for (TrainDeparture trainDeparture : trainDepartures) {
      if (trainDeparture.getTrainNumber() == trainNumber) {
        trainDeparture.setTrack(track);
        schedule.put(trainNumber, track);
        System.out.println("Train " + trainNumber + " is assigned to track " + track);
        found = true;
        break;
      }
    }
    if (!found) {
      System.out.println("Train " + trainNumber + " is not found within the schedule.");
    }
  }

  /**
   * Method for assigning a delay to a train departure.
   *
   * @param trainNumber The train number to check for.
   * @param delay The delay to check for.
   * @throws IllegalArgumentException If the delay is negative.
   */
  public void assignDelay(int trainNumber, Duration delay) {
    if (delay.isNegative()) {
      throw new IllegalArgumentException("Delay cannot be negative.");
    }
    if (schedule.containsKey(trainNumber)) {
      Duration currentDelay = delays.getOrDefault(trainNumber, Duration.ofMinutes(0));
      Duration newDelay = currentDelay.plus(delay);
      delays.put(trainNumber, newDelay);
      System.out.println("Delay of " + delay + " minutes assigned to train " + trainNumber + ".");
      System.out.println("Total delay for train " + trainNumber + ","
              + " is: " + newDelay + " minutes.");
      TrainDeparture trainDeparture = findDepartureByTrainNumber(trainNumber);
      if (trainDeparture != null) {
        trainDeparture.setDelay(newDelay);
      }
    } else {
      System.out.println("Train " + trainNumber + " is not found within the schedule.");
    }
  }

  /**
   * Method for finding a train departure by train number.
   *
   * @param trainNumber The train number to check for.
   * @return The train departure with the given train number,
   *        or null if no such train departure exists.
   */
  public TrainDeparture findDepartureByTrainNumber(int trainNumber) {
    for (TrainDeparture trainDeparture : trainDepartures) {
      if (trainDeparture.getTrainNumber() == trainNumber) {
        return trainDeparture;
      }
    }
    return null;
  }

  /**
   * Method for finding train departures by their destination.
   *
   * @param destination The destination to check for.
   * @return A list of train departures with the given destination.
   */
  public ArrayList<TrainDeparture> findDeparturesByDestination(String destination) {
    ArrayList<TrainDeparture> foundDepartures = new ArrayList<>();
    for (TrainDeparture trainDeparture : trainDepartures) {
      if (trainDeparture.getDestination().equalsIgnoreCase(destination.trim())) {
        foundDepartures.add(trainDeparture);
      }
    }
    return foundDepartures;
  }

  /**
   * An accessor method for the list of registered train departures.
   *
   * @return The list of registered train departures.
   */
  public ArrayList<TrainDeparture> getTrainDepartures() {
    return trainDepartures;
  }

  /**
   * Method for setting the manual time.
   * Help from ChatGPT.
   *
   * @param time The time to check for.
   */
  public void setManualTime(LocalTime time) {
    if (!firstManualTimeSet || (time != null && time.isAfter(manualTime))) {
      manualTime = time != null ? time : LocalTime.now();
      firstManualTimeSet = true;
    }
  }

  /**
   * Method for checking if the first manual time has been set.
   * Help from ChatGPT.
   *
   * @return True if the first manual time has been set, false otherwise.
   */
  public boolean firstManualTimeSet() {
    return firstManualTimeSet;
  }

  /**
   * Method for getting the manually set time.
   *
   * @return The manually set time.
   */
  public LocalTime getManualTime() {
    return manualTime;
  }

  /**
   * Method for removing expired train departures.
   * Removes train departures based on the manually set time.
   */
  public void removeExpiredDepartures() {
    if (manualTime == null) {
      System.out.println("Manual time is not set. Please set a time using "
              + "the 'Update the current time' command.");
      return;
    }
    Iterator<TrainDeparture> iterator = trainDepartures.iterator();
    while (iterator.hasNext()) {
      TrainDeparture trainDeparture = iterator.next();
      LocalTime originalDepartureTime = trainDeparture.getOriginalDepartureTime();
      Duration delay = delays.getOrDefault(trainDeparture.getTrainNumber(),
              Duration.ofMinutes(0));
      LocalTime actualDepartureTime = originalDepartureTime.plus(delay);
      if (actualDepartureTime.isBefore(manualTime)) {
        System.out.println("Train " + trainDeparture.getTrainNumber() + " has departed.");
        iterator.remove();
      }
    }
  }
}
