package edu.ntnu.stud;

import org.junit.jupiter.api.Test;

import java.time.DateTimeException;
import java.time.Duration;
import java.time.LocalTime;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for the TrainDeparture class.
 * Asked ChatGPT for what to test for.
 */
public class TrainDepartureTest {

  /**
   * Tests that a valid train departure can be created.
   */
  @Test
  void testValidDepartureCreation() {
    LocalTime departureTime = LocalTime.of(12, 0);
    String line = "L2A3";
    int trainNumber = 123;
    String destination = "Trondheim";
    Duration delay = Duration.ofMinutes(12);
    TrainDeparture trainDeparture = new TrainDeparture(departureTime, line, trainNumber, destination, delay);
    assertEquals(trainDeparture.getDepartureTime(), departureTime.plus(delay));
    assertEquals(trainDeparture.getLine(), line);
    assertEquals(trainDeparture.getTrainNumber(), trainNumber);
    assertEquals(trainDeparture.getDestination(), destination);
    assertEquals(trainDeparture.getDelay(), delay);
  }

  /**
   * Tests that a train departure cannot be created with an invalid departure time.
   */
  @Test
  void testInvalidDepartureTimeCreation() {
    int invalidHour = 24;
    int invalidMinute = 60;

    try {
      LocalTime invalidDepartureTime = LocalTime.of(invalidHour, invalidMinute);
      fail("Expected DateTimeException to be thrown.");
    } catch (DateTimeException e){
      assertTrue(e.getMessage().contains("Invalid value for HourOfDay"));
    }
  }

  /**
   * Tests that a valid delay can be assigned to a train departure.
   */
  @Test
  void testValidDelayCreation() {
    LocalTime departureTime = LocalTime.of(12, 0);
    String line = "L2A3";
    int trainNumber = 123;
    String destination = "Trondheim";
    Duration delay = Duration.ofMinutes(12);
    TrainDeparture trainDeparture = new TrainDeparture(departureTime, line, trainNumber, destination, delay);
    assertEquals(trainDeparture.getDelay(), delay);
  }

  /**
   * Tests that a train departure cannot be created with a negative delay.
   */
  @Test
  void testNegativeDelayCreation() {
    LocalTime departureTime = LocalTime.of(12, 0);
    String line = "L2A3";
    int trainNumber = 123;
    String destination = "Trondheim";
    Duration delay = Duration.ofMinutes(-12);
    assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(departureTime, line, trainNumber, destination, delay));
  }

  /**
   * Tests that a valid track can be assigned to a train departure.
   */
  @Test
  void testValidTrackAssignment() {
    LocalTime departureTime = LocalTime.of(12, 0);
    String line = "L2A3";
    int trainNumber = 123;
    String destination = "Trondheim";
    Duration delay = Duration.ofMinutes(12);
    TrainDeparture trainDeparture = new TrainDeparture(departureTime, line, trainNumber, destination, delay);
    int track = 2;
    trainDeparture.setTrack(track);
    assertEquals(trainDeparture.getTrack(), track);
  }

  /**
   * Tests that a train departure cannot be assigned a negative track.
   */
  @Test
  void testNegativeTrackAssignment() {
    LocalTime departureTime = LocalTime.of(12, 0);
    String line = "L2A3";
    int trainNumber = 123;
    String destination = "Trondheim";
    Duration delay = Duration.ofMinutes(12);
    TrainDeparture trainDeparture = new TrainDeparture(departureTime, line, trainNumber, destination, delay);
    int track = -2;
    assertThrows(IllegalArgumentException.class, () -> trainDeparture.setTrack(track));
  }

  /**
   * Tests that a train departure can be compared to another train departure.
   */
  @Test
  void compareTrainDeparturesTest() {
    LocalTime departureTime1 = LocalTime.of(12, 0);
    LocalTime departureTime2 = LocalTime.of(14, 0);
    TrainDeparture trainDeparture1 = new TrainDeparture(departureTime1, "L2", 123, "Trondheim", Duration.ofMinutes(12));
    TrainDeparture trainDeparture2 = new TrainDeparture(departureTime2, "L3", 234, "Trondheim", Duration.ofMinutes(15));
    int result = trainDeparture1.compareTo(trainDeparture2);

    assertFalse(result > 0, "trainDeparture1 should be after trainDeparture2");
    assertFalse(result == 0, "trainDeparture1 should not be equal to trainDeparture2");
    assertTrue(result < 0, "trainDeparture1 should be before trainDeparture2");
  }
}
