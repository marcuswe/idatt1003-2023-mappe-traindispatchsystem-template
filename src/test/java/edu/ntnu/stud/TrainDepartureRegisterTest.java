package edu.ntnu.stud;

import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for the TrainDepartureRegister class.
 * Asked ChatGPT for what to test for.
 */
public class TrainDepartureRegisterTest {

  /**
   * Helper method for registering a train departure.
   *
   * @param trainDepartureRegister The train departure register.
   * @param departureTime The departure time.
   * @param line The line.
   * @param trainNumber The train number.
   * @param destination The destination.
   * @param delay The delay.
   */
  private void registerTrainDeparture(TrainDepartureRegister trainDepartureRegister,
                                      LocalTime departureTime, String line, int trainNumber,
                                      String destination, Duration delay) {
    TrainDeparture trainDeparture = new TrainDeparture(departureTime,
            line, trainNumber, destination, delay);
    trainDepartureRegister.registerTrainDeparture(trainDeparture);
  }

  /**
   * Tests that a train departure can be successfully registered.
   */
  @Test
  void testRegisterTrainDeparture() {
    TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
    LocalTime departureTime = LocalTime.of(12, 0);
    String line = "L1";
    int trainNumber = 123;
    String destination = "Trondheim";
    Duration delay = Duration.ofMinutes(12);
    TrainDeparture trainDeparture = new TrainDeparture(departureTime, line,
            trainNumber, destination, delay);
    boolean result = trainDepartureRegister.registerTrainDeparture(trainDeparture);
    assertTrue(result, "Departure should be successfully registered.");
    assertEquals(1, trainDepartureRegister.getTrainDepartures().size(),
            "One train departure should be registered.");
  }

  /**
   * Tests that a track can be assigned to a departure.
   */
  @Test
  void testAssignTrack() {
    TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
    LocalTime departureTime = LocalTime.of(12, 0);
    String line = "L2";
    int trainNumber = 123;
    String destination = "Trondheim";
    Duration delay = Duration.ofMinutes(12);
    TrainDeparture trainDeparture = new TrainDeparture(departureTime,
            line, trainNumber, destination, delay);
    trainDepartureRegister.registerTrainDeparture(trainDeparture);
    trainDepartureRegister.assignTrack(trainNumber, 2);
    assertEquals(2, trainDeparture.getTrack(),
            "Track should be assigned to train departure.");
  }

  /**
   * Tests that a delay can be assigned to a departure.
   */
  @Test
  void testAssignDelay() {
    TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
    LocalTime departureTime = LocalTime.of(12, 0);
    String line = "A4";
    int trainNumber = 123;
    String destination = "Trondheim";
    Duration delay = Duration.ofMinutes(12);
    TrainDeparture trainDeparture = new TrainDeparture(departureTime,
            line, trainNumber, destination, delay);
    trainDepartureRegister.registerTrainDeparture(trainDeparture);
    trainDepartureRegister.assignDelay(trainNumber, Duration.ofMinutes(5));
    assertEquals(Duration.ofMinutes(5), trainDeparture.getDelay(),
            "Delay should be assigned to train departure.");
  }

  /**
   * Tests that a train departure can be found by the train number.
   */
  @Test
  void testFindDepartureByTrainNumber() {
    TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
    LocalTime departureTime = LocalTime.of(12, 0);
    String line = "L3";
    int trainNumber = 123;
    String destination = "Trondheim";
    Duration delay = Duration.ofMinutes(12);
    TrainDeparture trainDeparture = new TrainDeparture(departureTime, line,
            trainNumber, destination, delay);
    trainDepartureRegister.registerTrainDeparture(trainDeparture);
    TrainDeparture result = trainDepartureRegister.findDepartureByTrainNumber(trainNumber);
    assertEquals(trainDeparture, result, "Train departure should be found by train number.");
  }

  /**
   * Tests that a train departure can be found by the destination.
   */
  @Test
  void testFindDeparturesByDestination() {
    TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
    registerTrainDeparture(trainDepartureRegister, LocalTime.of(12, 0),
            "L2", 123, "Trondheim", Duration.ofMinutes(12));
    registerTrainDeparture(trainDepartureRegister, LocalTime.of(14, 0),
            "L3", 234, "Oslo", Duration.ofMinutes(15));
    registerTrainDeparture(trainDepartureRegister, LocalTime.of(16, 0),
            "L4", 345, "Bergen", Duration.ofMinutes(18));
    registerTrainDeparture(trainDepartureRegister, LocalTime.of(13, 0),
            "S2", 11, "Trondheim", Duration.ofMinutes(3));
    ArrayList<TrainDeparture> departuresToTrondheim = trainDepartureRegister.
            findDeparturesByDestination("Trondheim");
    ArrayList<TrainDeparture> departuresToOslo = trainDepartureRegister.
            findDeparturesByDestination("Oslo");
    assertEquals(2, departuresToTrondheim.size(),
            "Two departures should be found to Trondheim.");
    assertEquals(1, departuresToOslo.size(),
            "One departure should be found to Oslo.");
    assertEquals("Trondheim", departuresToTrondheim.get(0).getDestination(),
            "Destination should be Trondheim.");
    assertEquals("Oslo", departuresToOslo.get(0).getDestination(),
            "Destination should be Oslo.");
  }

  /**
   * Tests that the time can be manually set.
   */
  @Test
  void testSetManualTime() {
    TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
    LocalTime manualTime = LocalTime.of(12, 0);
    trainDepartureRegister.setManualTime(manualTime);
    assertTrue(trainDepartureRegister.firstManualTimeSet(),
            "First manual time should be set.");
    assertEquals(manualTime, trainDepartureRegister.getManualTime(),
            "Stored manual time should match the set time.");
  }

  /**
   * Tests that expired departures get removed when the time is updated past the departure time.
   */
  @Test
  void testRemoveExpiredDepartures() {
    TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
    LocalTime originalDepartureTime = LocalTime.of(12, 0);
    int trainNumber = 123;
    String destination = "Trondheim";
    Duration delay = Duration.ofMinutes(12);
    TrainDeparture trainDeparture = new TrainDeparture(originalDepartureTime,
            "L2", trainNumber, destination, delay);
    trainDepartureRegister.registerTrainDeparture(trainDeparture);
    trainDepartureRegister.assignDelay(trainNumber, delay);
    LocalTime manualTime = LocalTime.of(12, 30);
    trainDepartureRegister.setManualTime(manualTime);
    trainDepartureRegister.removeExpiredDepartures();
    ArrayList<TrainDeparture> remainingDepartures = trainDepartureRegister.getTrainDepartures();
    assertTrue(remainingDepartures.isEmpty(),
            "No departures should remain after removal of expired departures.");
  }

  /**
   * Tests that a train departure cannot be created if a departure with the
   * same train number already exists.
   */
  @Test
  void testDuplicateTrainDeparture() {
    TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
    registerTrainDeparture(trainDepartureRegister, LocalTime.of(12, 0),
            "L2", 123, "Trondheim", Duration.ofMinutes(12));
    registerTrainDeparture(trainDepartureRegister, LocalTime.of(14, 0),
            "L4", 123, "Oslo", Duration.ofMinutes(10));
    ArrayList<TrainDeparture> departures = trainDepartureRegister.getTrainDepartures();
    assertEquals(1, departures.size(), "Only one departure should be registered.");
  }

  /**
   * Tests that a track cannot be assigned to a non-existing train departure.
   */
  @Test
  void testAssignTrackToNonExistingTrainDeparture() {
    TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
    registerTrainDeparture(trainDepartureRegister, LocalTime.of(12, 0),
            "L2", 123, "Trondheim", Duration.ofMinutes(12));
    trainDepartureRegister.assignTrack(234, 2);
    ArrayList<TrainDeparture> departures = trainDepartureRegister.getTrainDepartures();
    assertEquals(1, departures.size(), "Only one departure should be registered.");
  }

  /**
   * Tests that a negative delay cannot be assigned to a departure.
   */
  @Test
  void testAssignNegativeDelay() {
    TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
    registerTrainDeparture(trainDepartureRegister, LocalTime.of(12, 0),
            "L2", 123, "Trondheim", Duration.ofMinutes(12));
    assertThrows(IllegalArgumentException.class, () -> trainDepartureRegister.
            assignDelay(123, Duration.ofMinutes(-5)));
    assertEquals(1, trainDepartureRegister.getTrainDepartures().size(),
            "Only one departure should be registered.");
  }

  /**
   * Tests that a train departure is not found if the train number does not exist.
   */
  @Test
  void testFindNonExistingDepartureByTrainNumber() {
    TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
    registerTrainDeparture(trainDepartureRegister, LocalTime.of(12, 0),
            "L2", 123, "Trondheim", Duration.ofMinutes(12));
    TrainDeparture trainDeparture = trainDepartureRegister.findDepartureByTrainNumber(234);
    assertNull(trainDeparture, "No departure should be found.");
  }

  /**
   * Tests that no departures are found if the destination does not exist.
   */
  @Test
  void testFindDeparturesByNonExistingDestination() {
    TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
    registerTrainDeparture(trainDepartureRegister, LocalTime.of(12, 0),
            "L2", 123, "Trondheim", Duration.ofMinutes(12));
    ArrayList<TrainDeparture> departures = trainDepartureRegister.findDeparturesByDestination("Oslo");
    assertTrue(departures.isEmpty(), "No departures should be found.");
  }

  /**
   * Tests that the time cannot be manually set to the past.
   */
  @Test
  void testSetManualTimeToPast() {
    TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
    LocalTime manualTime = LocalTime.of(12, 0);
    trainDepartureRegister.setManualTime(manualTime);
    assertTrue(trainDepartureRegister.firstManualTimeSet(),
            "First manual time should be set.");
    assertEquals(manualTime, trainDepartureRegister.getManualTime(),
            "Stored manual time should match the set time.");
    LocalTime newManualTime = LocalTime.of(11, 0);
    trainDepartureRegister.setManualTime(newManualTime);
    assertEquals(manualTime, trainDepartureRegister.getManualTime(),
            "Stored manual time should not change.");
  }

  /**
   * Tests that the program does not remove expired departures is the manual time is not set.
   */
  @Test
  void testRemoveExpiredDeparturesWithNoManualTimeSet() {
    TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
    LocalTime originalDepartureTime = LocalTime.of(12, 0);
    int trainNumber = 123;
    String destination = "Trondheim";
    Duration delay = Duration.ofMinutes(12);
    TrainDeparture trainDeparture = new TrainDeparture(originalDepartureTime,
            "L2", trainNumber, destination, delay);
    trainDepartureRegister.registerTrainDeparture(trainDeparture);
    trainDepartureRegister.assignDelay(trainNumber, delay);
    trainDepartureRegister.removeExpiredDepartures();
    ArrayList<TrainDeparture> remainingDepartures = trainDepartureRegister.getTrainDepartures();
    assertEquals(1, remainingDepartures.size(),
            "One departure should remain after removal of expired departures.");
  }
}
